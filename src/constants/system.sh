#!/usr/bin/env bash

# LINUX
declare -Ar MANAGERS=(
	['fedora']='dnf'
	['centos']='yum'
	['archlinux']='pacman'
	['debian']='apt-get'
)
declare DISTRIBUTION=$(grep -E '^ID=' /etc/os-release | sed -e 's/ID=//g' | sed -e 's/"//g')
declare VERSION=$(grep -E '^VERSION_ID=' /etc/os-release | sed -e 's/VERSION_ID=//g' | sed -e 's/"//g')
declare MANAGER=${MANAGERS[${DISTRIBUTION}]}
if [[ ! -z $(uname -m | grep '64') ]]; then
	declare ARCHITECTURE='x64'
else
	declare ARCHITECTURE='x32'
fi

# FILES
declare -Ar DEFAULT_PERMISSIONS=(
	['files']='644'
	['directories']='755'
)
declare -Ar SSH_PERMISSIONS=(
	['files']='600'
	['directories']='700'
)
