#!/usr/bin/env bash

declare DIRECTORY=$(dirname ${PWD})
declare JQ=${DIRECTORY}'/bin/jq'
declare CONFIG=${DIRECTORY}'/config/app.json'
declare CACHE=${DIRECTORY}'/var/cache/app.json'
declare -Ar HTTP_SERVERS=(
	['nginx']='NGINX'
	['apache']='Apache'
)
declare -Ar DATABASES=(
	['mysql']='MySQL'
	['postgresql']='PostgreSQL'
	['mongodb']='MongoDB'
)
