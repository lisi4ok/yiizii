#!/usr/bin/env bash

. ${PWD}/constants/main.sh
. ${PWD}/constants/system.sh
. ${PWD}/constants/console.sh

. ${PWD}/helpers/string.sh
. ${PWD}/helpers/notifications.sh
