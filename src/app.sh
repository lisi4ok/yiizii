#!/usr/bin/env bash

. ${PWD}/loader.sh

help () {
	info 'Helper Description.'
}

cache () {
	if [[ ${1} == 'init' ]]; then
		if [[ ! -d ${DIRECTORY}'/var/cache' ]]; then
			mkdir -p ${DIRECTORY}'/var/cache'
		fi
		if [[ ! -f ${CACHE} ]]; then
			local JSON='{"init":{"status":false,"when":""}}'
			echo ${JSON} | ${JQ} -r --indent 4 --tab "." > ${CACHE}
			success 'The cache has been successfully initialized.'
		elif [[ ${2} == 'up' && $(${JQ} ".init.status" ${CACHE}) == false ]]; then
			local JSON=$(${JQ} -c ".init.status = true" ${CACHE})
			JSON=$(echo ${JSON} | ${JQ} -c ".init.when = \"${DATE}\"")
			echo ${JSON} | ${JQ} -r --indent 4 --tab "." > ${CACHE}
			success 'The cache has been successfully updated.'
		fi
	elif [[ ${1} == 'remove' && -f ${CACHE} ]]; then
		rm ${CACHE}
		success 'The cache has been successfully removed.'
	elif [[ ${1} == 'up' && ! -z ${2} && $(${JQ} ".\"${2}\".status" ${CACHE}) == false ]]; then
		local JSON=$(${JQ} -c ".\"${2}\".status = true" ${CACHE})
		JSON=$(echo ${JSON} | ${JQ} -c ".\"${2}\".when = \"${DATE}\"")
		echo ${JSON} | ${JQ} -r --indent 4 --tab "." > ${CACHE}
		success 'The cache has been successfully updated.'
	elif [[ ${1} == 'down' && ! -z ${2} && $(${JQ} ".\"${2}\".status" ${CACHE}) == true ]]; then
		local JSON=$(${JQ} -c ".\"${2}\".status = false" ${CACHE})
		JSON=$(echo ${JSON} | ${JQ} -c ".\"${2}\".when = \"${DATE}\"")
		echo ${JSON} | ${JQ} -r --indent 4 --tab "." > ${CACHE}
		success 'The cache has been successfully updated.'
	fi
}

init () {
	if [[ ! -f ${CACHE} ]]; then
		if [[ ! -d ${DIRECTORY}'/var/tmp' ]]; then
			mkdir -p ${DIRECTORY}'/var/tmp'
		fi
		cache init
	fi
	if [[ $(${JQ} ".init.status" ${CACHE}) == false ]]; then
		cache init up
		info 'Preparing the application...'

		chmod ${DEFAULT_PERMISSIONS['directories']} $(find ${DIRECTORY}'/bin' -type d)
		chmod ${DEFAULT_PERMISSIONS['directories']} $(find ${DIRECTORY}'/resources' -type d)
		chmod ${DEFAULT_PERMISSIONS['directories']} $(find ${DIRECTORY}'/src' -type d)
		chmod ${DEFAULT_PERMISSIONS['directories']} $(find ${DIRECTORY}'/var' -type d)
		chmod ${DEFAULT_PERMISSIONS['directories']} $(find ${DIRECTORY}'/vendor' -type d)

		chmod ${DEFAULT_PERMISSIONS['files']} $(find ${DIRECTORY}'/bin' -type f)
		chmod ${DEFAULT_PERMISSIONS['files']} $(find ${DIRECTORY}'/resources' -type f)
		chmod ${DEFAULT_PERMISSIONS['files']} $(find ${DIRECTORY}'/src' -type f)
		chmod ${DEFAULT_PERMISSIONS['files']} $(find ${DIRECTORY}'/var' -type f)
		chmod ${DEFAULT_PERMISSIONS['files']} $(find ${DIRECTORY}'/vendor' -type f)

		cp $(find ${DIRECTORY}'/vendor/jq/linux-'${ARCHITECTURE}) ${JQ}
		chown $(whoami):$(whoami) ${JQ}
		chmod +x ${JQ}
		success 'The application has been successfully initialized.'
	else
		warning 'The application is already initialized.'
	fi
}

install () {
	if [[ -z ${1} ]]; then
		help
	elif [[ $(lowercase ${1}) == 'php' ]]; then
		echo $(lowercase ${1})
	elif [[ $(lowercase ${1}) == 'http' ]]; then
		echo $(lowercase ${1})
	elif [[ $(lowercase ${1}) == 'database' ]]; then
		echo $(lowercase ${1})
	elif [[ $(lowercase ${1}) == 'composer' ]]; then
		echo $(lowercase ${1})
	fi
}

#/**
# * Application
# */
if [[ -z ${1} ]]; then
	if [[ -f ${CACHE} ]]; then
		help
	else
		init
	fi
elif [[ $(lowercase ${1}) == 'init' ]]; then
	init
elif [[ $(lowercase ${1}) == 'install' ]]; then
	if [[ ! -z ${2} ]]; then
		install $(lowercase ${2})
	else
		install
	fi
fi
