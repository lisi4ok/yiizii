#!/usr/bin/env bash

notify () {
	if [[ ${1} == 'success' ]]; then
		echo -e ${CLEAR}${COLOR_GREEN}${2}${CLEAR}
	elif [[ ${1} == 'error' ]]; then
		echo -e ${CLEAR}${COLOR_RED}${2}${CLEAR}
	elif [[ ${1} == 'info' ]]; then
		echo -e ${CLEAR}${COLOR_BLUE}${2}${CLEAR}
	elif [[ ${1} == 'warning' ]]; then
		echo -e ${CLEAR}${COLOR_YELLOW}${2}${CLEAR}
	elif [[ ! -z ${1} ]]; then
		echo -e ${1}
	fi
}

message () {
	notify ${1} "${2}"
}

success () {
	notify success "${1}"
}

error () {
	notify error "${1}"
}

info () {
	notify info "${1}"
}

warning () {
	notify warning "${1}"
}
