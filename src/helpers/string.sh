#!/usr/bin/env bash

lowercase () {
	if [[ ! -z ${1} ]]; then
		echo $(echo "${1}" | sed -e 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/')
	fi
}

uppercase () {
	if [[ ! -z ${1} ]]; then
		echo $(echo "${1}" | sed -e 'y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/')
	fi
}

switchcase () {
	if [[ ! -z ${1} ]]; then
		echo $(echo "${1}" | sed -e 'y/abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/')
	fi
}
